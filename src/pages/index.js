import React from 'react';
import { Helmet } from 'react-helmet';

import About from '../components/about';
import Home from '../components/home';
import Navigation from '../components/navigation';

export default () => {
  return (
    <div className="signoff-website">
      <Helmet>
        <title>Signoff Solutions</title>
        <meta name="description" content="Signoff Solutions - automated testing specialists" />
        <meta name="keywords" content="automated testing, steve patterson" />
      </Helmet>
      <Navigation />
      <Home />
      <About />
    </div>
  );
}

import React from 'react';
import Typed from 'react-typed';

import background from '../images/bg_home_lg.jpg';

export default () => (
  <section className="bg-home" style={{
    backgroundImage: `url(${background})`
  }} id="home">
    <div className="home-center">
      <div className="home-desc-center">
        <div className="container">
          <div className="row pt-70">
            <div className="col-lg-8">
              <div className="title-heading">
                <h1 className="text-white">Steve Patterson</h1>
                <h2 className="text-white mb-4">I am a <Typed className='element text-custom' loop strings={['Software Developer-in-Test', 'Test Automation Engineer']} typeSpeed={50} backSpeed={40} />
                </h2>
                <p className="text-light mx-auto">I am an experienced full-stack Software Developer-in-Test (SDET) who
                  has worked on cross-functional Agile teams in development, testing
                  and management roles.</p>
                {/*<div className="mt-3">*/}
                {/*  <a href="#" className="btn btn-custom-white mr-3">View Resume</a>*/}
                {/*  <a href="#" className="btn btn-custom">Hire me</a>*/}
                {/*</div>*/}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

import React from 'react';

import logoBlack from '../images/logo_header_black_text.png';
import logoWhite from '../images/logo_header_white_text.png';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scroll: 0
    };
    this.handleScroll = this.handleScroll.bind(this);
  }

  handleScroll() {
    this.setState({
      scroll: window.scrollY
    });
  }

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll)
  }

  render () {
    const { scroll } = this.state;

    return (
      <nav className={`navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark ${scroll >= 50 ? 'nav-sticky' : ''}`}>
        <div className="container">
          <a className="navbar-brand logo" href="index.html">
            <img src={ logoBlack } alt="missing_logo" className="l-dark" height="78" />
            <img src={ logoWhite } alt="missing_logo" className="l-light" height="78" />
          </a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                  aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i className="mdi mdi-menu" />
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <ul className="navbar-nav ml-auto navbar-center" id="mySidenav">
              <li className="nav-item active">
                <a href="#home" className="nav-link">Home</a>
              </li>
              {/*<li className="nav-item">*/}
              {/*  <a href="#experience" className="nav-link">Experience</a>*/}
              {/*</li>*/}
              {/*<li className="nav-item">*/}
              {/*  <a href="#service" className="nav-link">Services</a>*/}
              {/*</li>*/}
              {/*<li className="nav-item">*/}
              {/*  <a href="#work" className="nav-link">Project</a>*/}
              {/*</li>*/}
              {/*<li className="nav-item">*/}
              {/*  <a href="#testimonial" className="nav-link">Reviews</a>*/}
              {/*</li>*/}
              {/*<li className="nav-item">*/}
              {/*  <a href="#blog" className="nav-link last-elements">blog</a>*/}
              {/*</li>*/}
              {/*<li className="nav-item">*/}
              {/*  <a href="#contact" className="nav-link last-elements">Contact</a>*/}
              {/*</li>*/}
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navigation;

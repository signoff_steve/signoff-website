import React from 'react';

import steve from '../images/about/about_steve.jpg';

export default () => (
  <section className="section">
    <div className="container">
      <div className="row">
        <div className="col-lg-4 col-md-6">
          <div className="mr-lg-3 mb-30">
            <img src={ steve } className="img-fluid" alt="" />
          </div>
        </div>

        <div className="col-lg-4 col-md-6">
          <div className="section-title mb-30">
            <h6>About us</h6>
            <h3 className="f-22">who am i ?</h3>
            <div className="spacer-15"></div>
            <p className="text-muted">I am an experienced full-stack Software Developer-in-Test (SDET) who has worked on cross-functional Agile teams in development, testing and management roles. I have hands-on experience across the delivery chain, including build and operational activities, and possess a pragmatic, results focused approach to my work.</p>
            <div className="about-personal">
              <ul className="list-unstyled">
                <li><span className="font-weight-bold title-head">Name : </span> <span> Steve Patterson </span></li>
                <li><span className="font-weight-bold title-head">Email ID : </span> <span> <a href="mailto:steve@signoffsolutions.com">steve@signoffsolutions.com</a> </span></li>
                <li><span className="font-weight-bold title-head">Website : </span> <span> <a href="https://www.signoffsolutions.com">www.signoffsolutions.com</a> </span></li>
              </ul>
            </div>
            <div className="mt-3">
              <a href="https://signoff-gitlab-build-artifacts.s3.eu-west-2.amazonaws.com/cv.pdf" download="steve_patterson_cv.pdf" className="btn btn-custom">Download resume</a>
            </div>
          </div>
        </div>

        <div className="col-lg-4 col-md-12">
          <div className="section-title">
            <h6>Skills</h6>
            <h3 className="f-22">Programming Languages</h3>
            <div className="spacer-15"></div>
            <p className="text-muted">I have experience building and maintaining automated test frameworks in the following languages:</p>
            <div className="spacer-15"></div>
          </div>
          <div className="skills-progress">
            <div className="progressbox">
              <h6 className="font-weight-normal">JavaScript</h6>
              <div className="progresses bg-dark">
                <div className="progresses-bar" role="progressbar" style={{width: '90%'}} aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                  <span className="text-muted">90%</span>
                </div>
              </div>
            </div>
            <div className="progressbox">
              <h6 className="font-weight-normal">C#</h6>
              <div className="progresses bg-dark">
                <div className="progresses-bar" role="progressbar" style={{width: '80%'}} aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                  <span className="text-muted">80%</span>
                </div>
              </div>
            </div>
            <div className="progressbox">
              <h6 className="font-weight-normal">Java</h6>
              <div className="progresses bg-dark">
                <div className="progresses-bar" role="progressbar" style={{width: '65%'}} aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                  <span className="text-muted">65%</span>
                </div>
              </div>
            </div>
            <div className="progressbox">
              <h6 className="font-weight-normal">Ruby</h6>
              <div className="progresses bg-dark">
                <div className="progresses-bar" role="progressbar" style={{width: '60%'}} aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                  <span className="text-muted">60%</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
)
